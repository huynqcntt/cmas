/**
 * Created by HuyNQ on 8/18.
 * Testing: USB 3G Mobifone (19d2:2003 ZTE WCDMA Technologies MSM)
 _SetTextMode = a_
 _SetPhoneNo = b_
 _SetMessage = c_
 _ClearAT = d_
 _SendSMS = e_
 _SendAlarmSms = f_
 */
const __sP = require("serialport"),__fs = require('fs');
function SMS(config) {
    // To enable SMS commented those code below
	var _me=this,_sms,_lists=[],_cf=JSON.parse(__fs.readFileSync('./config.json','utf8')),_op=config||{},_port=_op.port||_cf.serialport,_baudrate=_op.baudrate||_cf.baudrate,
	_sPort=new __sP(_port,{baudRate:_baudrate,dataBits:8,parity:'none',stopBits:1,flowControl:false,xon:false,rtscts:false,xoff:false,xany:false,buffersize:0});
	_sPort.on("open", function() {_cf.debug&&console.log('Serial communication open')});
	function _SetTextMode(me) {
		try{_sPort.write("AT+CMGF = 1" + String.fromCharCode(13),function(err){return err?console.log('Error on write: ',err.message):(console.log('CMGF ok'),_me)})}catch(err){console.log(err)}
    }function _SetPhoneNo(phone) {
		try{_sPort.write('AT+CMGS = "' + phone +'"' + String.fromCharCode(13),function(err){return err?console.log('Error on write: ',err.message):(console.log('CMGS ok'),_me)})}catch(err){console.log(err)}
    }function _SetMessage(msg) {
		try{_sPort.write(msg + String.fromCharCode(26),function(err){return err?console.log('Error on write: ',err.message):(console.log('Message ok' + msg),_me)})}catch(err){console.log(err)}
    }function _ClearAT(msg) {
		try{_sPort.write('AT' + String.fromCharCode(13),function(err) {return err?console.log('Error on write: ',err.message):(console.log('AT ok' + msg),_me)})}catch(err){console.log(err)}
    }function _SendSMS(message, phoneNo) {
        try{return _ClearAT(message,_SetMessage(message,_SetPhoneNo(phoneNo,_SetTextMode(_me))))}catch(err){console.log(err)}
    }function _SendAlarmSms(e){var s,l=_cf.Phones;for(s=0;s<l.length;s++)if(Tel:l[s].Enable && Tel:l[s].Tel !== ""){_me.lists.push({Tel:l[s].Tel,msg:e})}}
	return setInterval(function(){_cf.debug&&console.log("__huy__"+_me.lists.length),0<_me.lists.length&&(_sms=_me.lists[0],_cf.debug&&console.log(_sms),_me.sendSMS(_sms.msg,_sms.Tel),_me.lists.splice(0,1))},2e3),
	_me.sendAlarmSms=_SendAlarmSms,_me.lists=_lists,_me.sendSMS=_SendSMS,_me
}module.exports = SMS;