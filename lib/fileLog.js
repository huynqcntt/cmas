/**
 * Created by HuyNQ on 9/18.
 * Testing:
 */

function fileLog(config) {
	const __M = require('moment'),__fs = require('fs');
	
	var _me = this,
	_op = config || {},
	_dir = _op.dir || "./logs",
	_ext = _op.ext || ".csv",
	_fN = _op.fileName || __M().format('MM-DD-YYYY'),
	_slog = _op.SysName || "./logs.txt";
	function appendFile(file,val){
		__fs.appendFile(file, val + '\r\n', function (err) {
		  if (err) throw err
		}); 
	}
    function newFileLog(val = '',fileN = _fN){
		__fs.writeFile(_dir + '/' + fileN + _ext, val + '\r\n', function (err) {
		  if (err) throw err
		});
		return fileN
	}
	
	function appendLog(val = '',fileN = _fN){
		_me.appendFile(_dir + '/' + fileN + _ext, val)
	}
	function smsLog(msg,tel){
		_me.appendFile(_slog, "----@Alarm: " +  __M().format('YYYY-MM-DD HH:mm:ss') + "----\r\nSend sms to " + tel + "\r\n" + msg + "\r\n---- End alarm ----")
	}
	function appendSysLog(msg){
		_me.appendFile(_slog,msg)
	}
	_me.newFileLog = newFileLog,_me.appendLog = appendLog,_me.SmsLog = smsLog,_me.appendSysLog = appendSysLog,_me.appendFile = appendFile;return _me
}
module.exports = fileLog;
