const  __mac = require('macaddress'),
    __request = require('request'),
    __fs = require('fs');

var _cf = JSON.parse(__fs.readFileSync('./config.sync.json', 'utf8'))||{};
//get Mac address save to file config
if(_cf.macAddress === ""){
    if(_cf.EthernetPort === ""){
        console.log("Error: Ethernet port null in file config.sync.json")
    }
    else {
        __mac.one(_cf.EthernetPort, function (err, mac) {
            _cf.macAddress = mac;
            __fs.writeFile('./config.sync.json', JSON.stringify(_cf), 'utf8',  function (err) {
                if (err) throw err
            }); //
        });
    }
}
else{
    console.log("mac address get ok")
}
//get token
var url = _cf.CMS.url + ':' + _cf.CMS.port + '/' + _cf.CMS.path + '/controller-tokens';
var post = __request.post({
    headers: {
        'Accept': 'application/json',
        'content-type' : 'application/x-www-form-urlencoded'
    },
    url:     url,
    body:    "macAddress=" + _cf.macAddress + "&serial=" + _cf.serial
}, function(error, response, body){
    console.log(body);
});

/*

Content-Type: application/x-www-form-urlencoded
* var request = require('request');
request.post({
  headers: {'content-type' : 'application/x-www-form-urlencoded'},
  url:     'http://localhost/test2.php',
  body:    "mes=heydude"
}, function(error, response, body){
  console.log(body);
});
* */